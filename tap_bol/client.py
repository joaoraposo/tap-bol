"""REST client handling, including BolStream base class."""

import requests
from typing import Any, Dict, Optional

from memoization import cached
from singer_sdk.streams import RESTStream

from tap_bol.auth import BolAuthenticator
from singer_sdk.exceptions import FatalAPIError, RetriableAPIError
import time


class BolStream(RESTStream):
    """Bol stream class."""

    # TODO: Set the API's base URL here:
    url_base = "https://api.bol.com/retailer"

    records_jsonpath = "$[*]"  # Or override `parse_response`.
    next_page_token_jsonpath = "$.next_page"  # Or override `get_next_page_token`.

    @property
    @cached
    def authenticator(self) -> BolAuthenticator:
        """Return a new authenticator object."""
        return BolAuthenticator.create_for_stream(self)

    @property
    def http_headers(self) -> dict:
        """Return the http headers needed."""
        headers = {}
        if "user_agent" in self.config:
            headers["User-Agent"] = self.config.get("user_agent")
        headers["Accept"] = "application/vnd.retailer.v10+json"
        return headers

    def get_next_page_token(
        self, response: requests.Response, previous_token: Optional[Any]
    ) -> Optional[Any]:
        """Return a token for identifying next page or None if no more pages."""

        if response.json() == {}:
            return None

        if previous_token is None:
            #First page is already fetched.
            return 2

        return previous_token + 1

    def get_url_params(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Dict[str, Any]:
        """Return a dictionary of values to be used in URL parameterization."""
        params: dict = {}
        if next_page_token:
            params["page"] = next_page_token
        return params
    
    def validate_response(self, response: requests.Response) -> None:
        
        if (
            response.status_code in self.extra_retry_statuses
            or 500 <= response.status_code < 600
        ):
            msg = self.response_error_message(response)
            headers = dict(response.headers)
            if "retry-after" in headers:
                retry_after = int(headers["retry-after"])
                time.sleep(retry_after) # Sleep for the amount of time the server tells us to
            raise RetriableAPIError(msg, response)
        elif 400 <= response.status_code < 500:
            msg = self.response_error_message(response)
            raise FatalAPIError(msg)

    def get_records(self, context: Optional[dict]):
        
        sync_shipments = self.config.get("sync_shipments")
        sync_shipments = True if sync_shipments != False else sync_shipments  

        if self.name == "shipments" and not sync_shipments:
            pass
        else:
            for record in self.request_records(context):
                transformed_record = self.post_process(record, context)
                if transformed_record is None:
                    continue
                yield transformed_record
